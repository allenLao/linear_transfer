#!/usr/bin/env python
import sys
import codecs

def extract(argv):
    src_in = codecs.open(argv[0], encoding='utf8', mode='r')
    tgt_in = codecs.open(argv[1], encoding='utf8', mode='r')
    w_in = codecs.open(argv[2], encoding='utf8', mode='r')
    size = int(argv[4])
    src_writer = codecs.open(argv[3]+'.src', encoding='utf8', mode='w')
    tgt_writer = codecs.open(argv[3]+'.tgt', encoding='utf8', mode='w')
    src_map = {}
    tgt_map = {}

    src_in.readline() #skip first line
    for line in src_in:
        line = line.strip()
        items = line.split()
        word = items[0]
        items = items[1:]
        src_map[word] = ' '.join(items)

    tgt_in.readline() #skip first line
    for line in tgt_in:
        line = line.strip()
        items = line.split()
        word = items[0]
        items = items[1:]
        tgt_map[word] = ' '.join(items)

    #read seeds
    s = 0
    for line in w_in:
        line = line.strip()
        items = line.split()
        src_word = items[0]
        tgt_word = items[1]
        print src_word, (src_word in src_map.keys()), tgt_word, (tgt_word in tgt_map.keys())
        if (src_word in src_map.keys()) and (tgt_word in tgt_map.keys()):
            src_writer.write(src_word + " " + src_map[src_word] + "\n")
            src_writer.flush()
            tgt_writer.write(tgt_word + " " + tgt_map[tgt_word] + "\n")
            tgt_writer.flush()
            s += 1
        if s >= size:
            break

    w_in.close()

    src_writer.flush()
    src_writer.close()
    tgt_writer.flush()
    tgt_writer.close()

if __name__ == "__main__":
    if len(sys.argv) != 6:
        print "Usage: <sourceVec> <targetVec> <seed> <prefix> <size>"
        sys.exit(1)

    extract(sys.argv[1:])
