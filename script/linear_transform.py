#!/usr/bin/env python
import sys
import codecs
import numpy as np
import numpy.linalg as la

class linear_transformation:
    """
    def __init__(self):
        print "hello, init linear transfomation"
    """
    def load(self, model_file):
        reader = codecs.open(model_file, 'r', 'utf8')
        matrix = []
        for line in reader:
            line = line.strip()
            items = line.split()
            vec = [ float(i) for i in items]
            matrix.append(vec)

        self.weight = np.mat(matrix)

    def save(self, mtx, model_file):
        writer = codecs.open(model_file, 'w', 'utf8')
        for row in mtx:
            for ele in row:
                writer.write(str(ele) + " ")
            writer.write("\n")
            writer.flush()

        writer.flush()
        writer.close()

    def predict(self, model_file, src_file, tgt_file, k_best, fout):
        self.load(model_file)
        src_words, src_vecs = self.read_vec(src_file)
        tgt_words, tgt_vecs = self.read_vec(tgt_file)
        trans_vecs = (self.weight * src_vecs.T).T
        k = min(k_best, len(tgt_words))

        writer = codecs.open(fout, 'w', 'utf8')
        for i in range(0, len(trans_vecs)):
            src_val = np.array(trans_vecs[i]).flatten().tolist()
            rvl = []
            for j in range(0, len(tgt_vecs)):
                tgt_val = np.array(tgt_vecs[j]).flatten().tolist()
                score = np.dot(src_val, tgt_val)
                src_norm = la.norm(src_val)
                tgt_norm = la.norm(tgt_val)
                score /= src_norm * tgt_norm
                rvl.append(score)

            idxs = np.array(rvl).argsort()[::-1][:k]
            bf = src_words[i] + " "
            for idx in idxs:
                bf += tgt_words[idx] + ":" +str(rvl[idx]) + " "

            writer.write(bf.strip() + "\n")
            writer.flush()

        writer.flush()
        writer.close()


    """
    Read word-vectors and return word_list, values
    """
    def read_vec(self,fin):
        read = codecs.open(fin, mode='r', encoding='utf8')
        words = []
        values = []
        for line in read:
            line = line.strip()
            items = line.split()
            words.append(items[0])
            items = items[1:]
            v = [float(i) for i in items]
            values.append(v)

        mtx = np.array(values)
        return (words, mtx)

    def train(self, src_vec, tgt_vec, maxIter, learning_rate, epsilon, model_file):
        src_words, src_values = self.read_vec(src_vec)
        tgt_words, tgt_values = self.read_vec(tgt_vec)
        self.weight = np.random.rand(tgt_values.shape[1], src_values.shape[1])
        it = 0
        old_err = float('inf')
        new_err = 0.0
        num_samples = tgt_values.shape[0]
        diff_err = abs(old_err - new_err)/num_samples
        while (it < maxIter) and (abs(old_err - new_err)/num_samples > epsilon):
            err = np.dot(self.weight, src_values.T) - tgt_values.T
            old_err = new_err
            new_err = la.norm(err)
            diff_err = abs(old_err - new_err)/num_samples
            print old_err/num_samples, new_err/num_samples, diff_err
            self.weight -= learning_rate * np.dot(err, src_values)
            it += 1

        self.save(self.weight, model_file)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage:train <sourceVec> <targetVec> <maxIteration> <learning_rate> <epsilon> <model>"
        print "Usage:predict <model> <sourceVec> <targetVec> <k-best> <output>"
        sys.exit(1)

    if sys.argv[1] == "train":
        ltrans = linear_transformation()
        ltrans.train(sys.argv[2], sys.argv[3],int(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]), sys.argv[7])

    elif sys.argv[1] == "predict":
        ltrans = linear_transformation()
        ltrans.predict(sys.argv[2], sys.argv[3], sys.argv[4], int(sys.argv[5]), sys.argv[6])


